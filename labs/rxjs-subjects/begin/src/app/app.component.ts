import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { SubjectsService } from './core/subjects.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit, OnDestroy {
  sub1: Subscription = new Subscription();
  sub2: Subscription = new Subscription();
  sub3: Subscription = new Subscription();

  subscription1Data = [];
  subscription2Data = [];
  subscription3Data = [];

  constructor(
    private readonly subjectsService: SubjectsService
  ) {}

  ngOnInit() {}

  subscribe1() {
    this.sub1.add(this.subjectsService.observable$.subscribe(
      data => this.subscription1Data.push(data)
    ));
  }

  subscribe2() {
    this.sub2.add(this.subjectsService.observable$.subscribe(
      data => this.subscription2Data.push(data)
    ));
  }

  subscribe3() {
    this.sub3.add(this.subjectsService.observable$.subscribe(
      data => this.subscription3Data.push(data)
    ));
  }

  ngOnDestroy() {
    this.sub1.unsubscribe();
    this.sub2.unsubscribe();
    this.sub3.unsubscribe();
  }

}
